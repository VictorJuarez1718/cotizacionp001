const http = require("http");
const express = require("express");
const bodyparser = require("body-parser");

const app = express();
app.set('view engine', "ejs")
app.use(express.static(__dirname + '/public'));
app.use(bodyparser.urlencoded ({extended:true}));



app.get('/', (req, res) => {
    const resultados = {
        valor:req.query.valor,
        pInicial:req.query.pInicial,
        plazos:req.query.plazos
       
    }
    
    res.render('index', resultados);
  });

app.post('/', (req, res)=>{
    const resultados = {
        
        valor:req.body.valor,
        pInicial:req.body.pInicial,
        plazos:req.body.plazos
    }
    res.render('index', resultados);
})

app.use((req, res, next)=>{
    res.status(404).sendFile(__dirname + '/public/error.html');
});

// Escuchar al servidor por el puerto 3000
const puerto = 3000;
app.listen(puerto, ()=>{
    console.log("iniciando puerto 3000 de la P001")
})